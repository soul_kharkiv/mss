#include <string.h>

#include "safeStrings.h"

void safeStrCpy(char **str, const char *src)
{
	safeStrRsz(str, strlen(*str) + strlen(src) + 1);
	strcpy(*str, src);
}
