#include <stdlib.h>
#include <string.h>

#include "safeStrings.h"

void safeStrRsz(char **str, unsigned int newSize)
{
	if (newSize == 0)
	{
		if (*str != 0)
			free(*str);
	}
	else
	{
		if (*str == 0)
		{
			*str = malloc(newSize);
		}
		else
		{
			char *buf;
			int oldSize = strlen(*str) + 1;
			buf = malloc(oldSize);
			strcpy(buf, *str);
			free(*str);
			*str = malloc(newSize);

			if (newSize > oldSize)
				newSize = oldSize;

			memcpy(*str, buf, newSize - 1);
			*(*str + newSize - 1) = '\0';

			free(buf);
		}
	}
}
