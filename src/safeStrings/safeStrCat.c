#include <string.h>

#include "safeStrings.h"

void safeStrCat(char **str, const char *src)
{
	safeStrRsz(str, strlen(*str) + strlen(src) + 1);
	strcat(*str, src);
}
