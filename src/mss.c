#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "argtable2.h"

#include "localization.h"
#include "safeStrings.h"
#include "screenshot.h"

#define PROGNAME "mss"
#define PROGVERSION "0.92"
#define EMAIL "bug@soul.kh.ua"

int main(int argc, char *argv[])
{
	int exit = 0;

	localizationInit("/usr/share/mss/localization/");

	int screenX = 0;
	int screenY = 0;
	int screenWidth = -1;
	int screenHeight = -1;
	char canStdOut = 0;
	char canFile = 1;
	int canTty = 0;
	char *path = safeStrCrt("");
	char *filename = safeStrCrt("");
	char *extension = safeStrCrt("");

	struct arg_lit *argHelp = arg_lit0(NULL, "help", localizationGetString("HELP_HELP_DES"));
	struct arg_lit *argVersion = arg_lit0("v", "version", localizationGetString("HELP_VERSION_DES"));
	struct arg_int *argX = arg_int0("x", NULL, NULL, localizationGetString("HELP_X_DES"));
	struct arg_int *argY = arg_int0("y", NULL, NULL, localizationGetString("HELP_Y_DES"));
	struct arg_int *argWidth = arg_int0("w", "width", NULL, localizationGetString("HELP_W_DES"));
	struct arg_int *argHeight = arg_int0("h", "height", NULL, localizationGetString("HELP_H_DES"));
	struct arg_lit *argTty = arg_lit0("t", "tty", localizationGetString("HELP_T_DES"));
	struct arg_lit *argStdOut = arg_lit0("o", "stdout", localizationGetString("HELP_O_DES"));
	struct arg_lit *argFile = arg_lit0("f", "file", localizationGetString("HELP_F_DES"));
	struct arg_file *argFilename = arg_file0(NULL, NULL, "name", localizationGetString("HELP_NAME_DES"));

	struct arg_end *end = arg_end(20);

	void *argtable[] = {argHelp, argVersion, argX, argY, argWidth, argHeight, argStdOut, argFile, argTty, argFilename, end};

	int nerrors;
	nerrors = arg_parse(argc, argv, argtable);
	if (nerrors > 0)
	{
		arg_print_errors(stdout, end, PROGNAME);
		printf(localizationGetString("ERROR_HELP"), PROGNAME);
		putchar('\n');
	}

	if (argHelp->count > 0)
	{
		printf(localizationGetString("HELP_USAGE_DES"), PROGNAME);
		arg_print_syntax(stdout, argtable, "\n\n");
		arg_print_glossary(stdout, argtable, "  %-30s %s\n");
		putchar('\n');
		printf(localizationGetString("HELP_REPORT_DES"), EMAIL);
		putchar('\n');
		exit = 1;
	}

	if (argVersion->count > 0)
	{
		printf(localizationGetString("VERSION_DES"), PROGNAME, PROGVERSION);
		putchar('\n');
		exit = 1;
	}

	if (!exit)
	{
		if (argX->count > 0)
			screenX = argX->ival[0];
		if (argY->count > 0)
			screenY = argY->ival[0];
		if (argWidth->count > 0)
			screenWidth = argWidth->ival[0];
		if (argHeight->count > 0)
			screenHeight = argHeight->ival[0];
		if (argStdOut->count > 0)
		{
			canStdOut = 1;
			canFile = 0;
		}
		if (argFile->count > 0)
			canFile = 1;
		if (argTty->count > 0)
			canTty = 1;

		if (argFilename->count > 0)
		{
			safeStrRsz(&path, strlen(argFilename->filename[0]) + 1);
			strcpy(path, argFilename->filename[0]);

			if (strchr(path, '/'))
			{
				safeStrRsz(&filename, strlen(strrchr(path, '/')) + 1);
				strcpy(filename, strrchr(path, '/') + 1);

				int i;
				for (i = strlen(path); path[i - 1] != '/'; i--);
				path[i] = '\0';
				safeStrRsz(&path, strlen(path) + 1);
			}
			else
			{
				safeStrRsz(&filename, strlen(path) + 1);
				strcpy(filename, path);
				safeStrRsz(&path, 1);
			}

			if (strchr(filename, '.'))
			{
				safeStrRsz(&extension, strlen(strrchr(filename, '.')) + 1);
				strcpy(extension, strrchr(filename, '.'));

				int i = strlen(filename);
				while(filename[i - 1] != '.')
					i--;
				filename[--i] = '\0';
				safeStrRsz(&filename, strlen(filename) + 1);
			}
		}

		char *tmpExtension = safeStrCrt(extension);
		safeStrTlr(tmpExtension);
		if (strcmp(tmpExtension, ".ppm") != 0 && strcmp(tmpExtension, ".jpeg") != 0 && strcmp(tmpExtension, ".jpg") != 0 && strcmp(tmpExtension, ".png") != 0)
		{
			if (strcmp(extension, "") != 0)
			{
				safeStrRsz(&filename, strlen(filename) + strlen(extension) + 1);
				strcat(filename, extension);
			}
			safeStrRsz(&extension, strlen(".png") + 1);
			strcpy(extension, ".png");
		}
		free(tmpExtension);

		if (!strcmp(filename, ""))
		{
			time_t t = time(NULL);
			struct tm* timeinfo = localtime(&t);
			safeStrRsz(&filename, strlen("YYYY-mm-dd-HHMMSS") + 1);
			sprintf(filename, "%04d-%02d-%02d-%02d%02d%02d", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
		}

		safeStrRsz(&path, strlen(path) + strlen(filename) + strlen(extension) + 1);
		sprintf(path, "%s%s%s", path, filename, extension);
		safeStrTlr(extension);

		screenshot *scr;
		if ((scr = scrCreate(screenX, screenY, screenWidth, screenHeight, canTty)) != 0)
		{
			char ernum;
			if ((ernum = scrSave(scr, canStdOut, canFile, path, extension + 1)) != 0)
			{
				printf("%s", localizationGetString("ERROR_FILE"));
				switch (ernum)
				{
					case 2:
						printf(": %s\n", localizationGetString("ERROR_DIRECTORY"));
						break;

					case 13:
						printf(": %s\n", localizationGetString("ERROR_PERMISSION"));
						break;

					case 20:
						printf(": %s\n", localizationGetString("ERROR_NOT_A_DIR"));
						break;

					default:
						putchar('\n');
						break;
				}
			}
			scrFree(scr);
		}
		else
		{
			printf("%s: %s\n", localizationGetString("ERROR_TTY"), localizationGetString("ERROR_PERMISSION"));
		}
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

	free(extension);
	free(filename);
	free(path);

	localizationFree();
	return 0;
}
