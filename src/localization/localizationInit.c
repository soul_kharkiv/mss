#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "localization.h"
#include "safeStrings.h"

char localizationInit(const char *dir)
{
	char result = 1;
	char *path = safeStrCrt(dir);

	localizationVars = 0;
	localizationStrings = 0;
	localizationStringsCount = 0;

	char *language = safeStrCrt(setlocale(LC_CTYPE, ""));

	if (strchr(language, '_'))
	{
		int i;
		for (i = 0; language[i] != '_'; i++);
		language[i] = '\0';
		safeStrRsz(&language, strlen(language) + 1);
	}

	safeStrCat(&path, language);
	free(language);

	FILE *localizationFile;
	if ((localizationFile = fopen(path, "r")) != 0)
	{
		int i = 0;
		while ((i = fgetc(localizationFile)) != EOF)
		{
			if (i == '\n')
				localizationStringsCount++;
		}

		if (localizationStringsCount)
		{
			localizationVars = malloc((localizationStringsCount) * sizeof(char*));
			localizationStrings = malloc((localizationStringsCount) * sizeof(char*));

			fseek(localizationFile, 0L, SEEK_SET);

			int *localizationStringSize = malloc(localizationStringsCount * sizeof(int));
			for (i = 0; i < localizationStringsCount; i++)
				localizationStringSize[i] = 0;

			for (i = 0; i < localizationStringsCount;)
			{
				localizationStringSize[i]++;
				if (fgetc(localizationFile) == '\n')
					i++;
			}

			fseek(localizationFile, 0L, SEEK_SET);

			i = 0;
			while (i < localizationStringsCount)
			{
				int j = 0;
				*(localizationVars + i) = malloc((localizationStringSize[i] + 1) * sizeof(char));
				while ((localizationVars[i][j++] = fgetc(localizationFile)) != '=');

				localizationVars[i][j] = '\0';
				safeStrRsz(&localizationVars[i], strlen(localizationVars[i]));

				j = 0;
				*(localizationStrings + i) = malloc((localizationStringSize[i] - strlen(localizationVars[i])) * sizeof(char));
				while ((localizationStrings[i][j++] = fgetc(localizationFile)) != '\n');
				localizationStrings[i][--j] = '\0';
				i++;
			}

			free(localizationStringSize);
			fclose(localizationFile);

			result = 0;
		}
	}
	free(path);

	return result;
}
