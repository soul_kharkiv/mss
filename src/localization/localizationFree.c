#include <stdlib.h>

#include "localization.h"

void localizationFree()
{
	int i;
	for (i = 0; i < localizationStringsCount; i++)
	{
		free(*(localizationVars + i));
		free(*(localizationStrings + i));
	}
	free(localizationVars);
	free(localizationStrings);
}
