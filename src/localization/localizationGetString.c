#include <stdio.h>

#include <stdlib.h>
#include <string.h>

#include "localization.h"
#include "safeStrings.h"


char *localizationGetString(char *var)
{
	char isFound = 0;
	char *result = var;
	int i;

	for (i = 0; i < localizationStringsCount && !isFound; i++)
		if (!strcmp(var, localizationVars[i]))
			isFound++;

	if (isFound)
		result = localizationStrings[i - 1];

	return result;
}
