#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "localization.h"
#include <safeStrings.h>
#include "screenshot.h"

char scrSave(screenshot *img, char canStdOut, char canFile, char *filename, char *extension)
{
	char ernum = 0;
	FILE *screenFile;
	screenFile = tmpfile();

	if (!strcmp(extension, "ppm"))
		scrSavePPM(img, screenFile);
	else if (!strcmp(extension, "jpg") || !strcmp(extension, "jpeg"))
		scrSaveJPEG(img, screenFile);
	else
		scrSavePNG(img, screenFile);

	if (canStdOut)
	{
		fseek(screenFile, 0L, SEEK_SET);
		int c;
		while ((c = fgetc(screenFile)) != EOF)
			fputc(c, stdout);
	}

	if (canFile)
	{
		FILE *file;

		if ((file = fopen(filename, "w")) != 0)
		{
			fseek(screenFile, 0L, SEEK_SET);

			int tmpChar;
			while ((tmpChar = fgetc(screenFile)) != EOF)
				fputc(tmpChar, file);

			fclose(file);
		}
		else
			ernum = errno;
	}

	fclose(screenFile);

	return ernum;
}
