#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <png.h>

#include "screenshot.h"

void scrSavePNG(screenshot *img, FILE *file)
{
	int i, j;
	png_structp pngStr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if (pngStr != NULL)
	{
		png_infop pngInfo = png_create_info_struct(pngStr);
		if (pngInfo != NULL)
		{
			if (!setjmp(png_jmpbuf(pngStr)))
			{
				unsigned short width = scrGetWidth(img);
				unsigned short height = scrGetHeight(img);
				int depth = 8;
				int pixelSize = 3;
				png_byte **pngColors = NULL;

				png_set_IHDR(pngStr, pngInfo, width, height, depth, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

				pngColors = png_malloc(pngStr, height * sizeof(png_byte*));

				for (i = 0; i < height; i++)
				{
					png_byte *row = png_malloc(pngStr, sizeof(uint8_t) * width * pixelSize);
					pngColors[i] = row;
					for (j = 0; j < width; j++)
					{
						screenshotPixel pixel = scrGetPixel(img, j, i);
						*row++ = pixGetRed(pixel);
						*row++ = pixGetGreen(pixel);
						*row++ = pixGetBlue(pixel);
					}
				}

				png_init_io(pngStr, file);
				png_set_rows(pngStr, pngInfo, pngColors);
				png_write_png(pngStr, pngInfo, PNG_TRANSFORM_IDENTITY, NULL);

				for (i = 0; i < height; i++)
					png_free(pngStr, pngColors[i]);

				png_free(pngStr, pngColors);
				png_destroy_write_struct(&pngStr, &pngInfo);
			}
		}
	}
}
