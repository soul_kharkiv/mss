#include <stdio.h>
#include <stdlib.h>

#include <jpeglib.h>

#include "screenshot.h"

void scrSaveJPEG(screenshot *img, FILE *file)
{
	unsigned short width = scrGetWidth(img);
	unsigned short height = scrGetHeight(img);
	screenshotPixel *colors = img->pixmap;
	int quality = 75;

	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);

	jpeg_stdio_dest(&cinfo, file);

	cinfo.image_width = width;
	cinfo.image_height = height;
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;

	jpeg_set_defaults(&cinfo);
	jpeg_set_quality(&cinfo, quality, 1);
	jpeg_start_compress(&cinfo, 1);

	JSAMPROW rowPointer[1];
	int rowStride = cinfo.image_width;

	while (cinfo.next_scanline < cinfo.image_height)
	{
		rowPointer[0] = (JSAMPLE*)(colors + cinfo.next_scanline * rowStride);
		jpeg_write_scanlines(&cinfo, rowPointer, 1);
	}

	jpeg_finish_compress(&cinfo);
	jpeg_destroy_compress(&cinfo);
}
