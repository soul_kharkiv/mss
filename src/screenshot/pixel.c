#include "screenshot.h"

unsigned char pixGetRed(screenshotPixel pixel)
{
	return pixel.red;
}

unsigned char pixGetBlue(screenshotPixel pixel)
{
	return pixel.blue;
}

unsigned char pixGetGreen(screenshotPixel pixel)
{
	return pixel.green;
}

void pixSetRed(screenshotPixel *pixel, unsigned char red)
{
	pixel->red = red;
}

void pixSetGreen(screenshotPixel *pixel, unsigned char green)
{
	pixel->green = green;
}

void pixSetBlue(screenshotPixel *pixel, unsigned char blue)
{
	pixel->blue = blue;
}
