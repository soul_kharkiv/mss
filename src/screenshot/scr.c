#include "screenshot.h"

unsigned short scrGetWidth(screenshot *img)
{
	return img->width;
}

unsigned short scrGetHeight(screenshot *img)
{
	return img->height;
}

screenshotPixel scrGetPixel(screenshot *img, unsigned short x, unsigned short y)
{
	return img->pixmap[x + (scrGetWidth(img) * y)];
}

void scrSetPixel(screenshot *img, unsigned short x, unsigned short y, screenshotPixel pixel)
{
	img->pixmap[x + (scrGetWidth(img) * y)] = pixel;
}
