#include <stdlib.h>

#include "screenshot.h"

void scrFree(screenshot *img)
{
		free(img->pixmap);
		free(img);
}
