#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

#include <fcntl.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "localization.h"
#include "screenshot.h"

screenshot *scrInit(unsigned short width, unsigned short height)
{
	screenshot *img;

	img = malloc(sizeof(screenshot));
	img->width = width;
	img->height = height;
	img->pixmap = malloc(width * height * sizeof(screenshotPixel));

	return img;
}

screenshot *scrCreate(int x, int y, unsigned short width, unsigned short height, int canTty)
{
	int i, j;
	screenshot *img;

	Display *display = XOpenDisplay(NULL);

	if (display == NULL || canTty)
	{
		int fbDev = open("/dev/fb0", O_RDONLY);

		if (fbDev > 0)
		{
			struct fb_var_screeninfo vinfo;
			struct fb_fix_screeninfo finfo;
			long size;
			unsigned char *fbPixs;

			ioctl(fbDev, FBIOGET_VSCREENINFO, &vinfo);
			ioctl(fbDev, FBIOGET_FSCREENINFO, &finfo);

			if (width < 0 || width + x > vinfo.xres)
				width = vinfo.xres - x;
			if (height < 0 || height + y > vinfo.yres)
				height = vinfo.yres - y;

			img = scrInit(width, height);
			size = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;
			fbPixs = mmap(0, size, PROT_READ, MAP_SHARED, fbDev, 0);

			for (i = 0; i < height; i++)
				for (j = 0; j < width; j++)
				{
					long pos = (x + j + vinfo.xoffset) * (vinfo.bits_per_pixel / 8) + (y + i + vinfo.yoffset) * finfo.line_length;
					screenshotPixel pixel;
					pixSetRed(&pixel, *(fbPixs + pos + 2));
					pixSetGreen(&pixel, *(fbPixs + pos + 1));
					pixSetBlue(&pixel, *(fbPixs + pos));
					scrSetPixel(img, j, i, pixel);
				}

			munmap(fbPixs, size);
			close(fbDev);
		}
		else
			img = 0;
	}
	else
	{
		Window window = DefaultRootWindow(display);

		int screenNum = DefaultScreen(display);

		if (width < 0 || width + x > DisplayWidth(display, screenNum))
			width = DisplayWidth(display, screenNum) - x;
		if (height < 0 || height + y > DisplayHeight(display, screenNum))
			height = DisplayHeight(display, screenNum) - y;

		img = scrInit(width, height);

		XImage *image = XGetImage(display, RootWindow(display, screenNum), x, y, width, height, XAllPlanes(), ZPixmap);

		for (i = 0; i < height; i++)
			for (j = 0; j < width; j++)
			{
				unsigned long xpixel = XGetPixel(image, j, i);
				screenshotPixel pixel;
				pixSetRed(&pixel, xpixel >> 16);
				pixSetGreen(&pixel, (xpixel & 0x00FF00) >> 8);
				pixSetBlue(&pixel, xpixel & 0x0000FF);
				scrSetPixel(img, j, i, pixel);
			}

		XDestroyImage(image);
		XDestroyWindow(display, window);
	}

	if (display != NULL)
		XCloseDisplay(display);

	return img;
}
