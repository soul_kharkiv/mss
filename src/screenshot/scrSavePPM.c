#include <stdio.h>

#include "screenshot.h"

void scrSavePPM(screenshot *img, FILE *file)
{
	unsigned short width = scrGetWidth(img);
	unsigned short height = scrGetHeight(img);
	fprintf(file, "P3\n%d %d\n255\n", width, height);
	int i, j;
	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++)
		{
			screenshotPixel pixel = scrGetPixel(img, j, i);
			fprintf(file, "%d %d %d\n", pixGetRed(pixel), pixGetGreen(pixel), pixGetBlue(pixel));
		}
}
