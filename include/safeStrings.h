#ifndef SAFESTRINGS_H
#define SAFESTRINGS_H

extern void safeStrCat(char **str, const char *src);
extern void safeStrCpy(char **str, const char *src);
extern char *safeStrCrt(const char *src);
extern void safeStrRsz(char **str, unsigned int newSize);
extern void safeStrTlr(char *str);

#endif
