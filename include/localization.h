#ifndef LOCALIZATION_H
#define LOCALIZATION_H

char **localizationVars;
char **localizationStrings;
int localizationStringsCount;

extern char *localizationGetString(char *var);
extern void localizationFree(void);
extern char localizationInit(const char *dir);

#endif
