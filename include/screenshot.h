#ifndef SCREENSHOT_H
#define SCREENSHOT_H

#include <stdio.h>

typedef struct
{
	unsigned char red;
	unsigned char green;
	unsigned char blue;
} screenshotPixel;

typedef struct __attribute__((packed, aligned(1)))
{
	unsigned short width;
	unsigned short height;
	screenshotPixel *pixmap;
} screenshot;

extern unsigned char pixGetRed(screenshotPixel pixel);
extern unsigned char pixGetBlue(screenshotPixel pixel);
extern unsigned char pixGetGreen(screenshotPixel pixel);

extern void pixSetRed(screenshotPixel *pixel, unsigned char red);
extern void pixSetGreen(screenshotPixel *pixel, unsigned char green);
extern void pixSetBlue(screenshotPixel *pixel, unsigned char blue);


extern unsigned short scrGetWidth(screenshot *img);
extern unsigned short scrGetHeight(screenshot *img);
extern screenshotPixel scrGetPixel(screenshot *img, unsigned short x, unsigned short y);

extern void scrSetPixel(screenshot *img, unsigned short x, unsigned short y, screenshotPixel pixel);

extern screenshot *scrCreate(int x, int y, unsigned short width, unsigned short height, int canTty);
extern char scrSave(screenshot *img, char canStdOut, char canFile, char *filename, char *format);
extern void scrFree(screenshot *img);

extern void scrSaveJPEG(screenshot *img, FILE *file);
extern void scrSavePPM(screenshot *img, FILE *file);
extern void scrSavePNG(screenshot *img, FILE *file);

#endif
