PROJECT_NAME=mss

BINDIR=bin
INCDIR=include
SRCDIR=src

CC=clang
STD=-std=c89
CFLAGS=$(STD) -O2
INC=-I $(INCDIR)
LIBS=-lm -lX11 -lpng -ljpeg -largtable2

SRCS=$(shell find $(SRCDIR) -name '*.c')
OBJS=$(SRCS:.c=.o)

BIN=$(BINDIR)/$(PROJECT_NAME)

.PHONY: all clean
.DEFAULT_GOAL: all

all: $(BIN)

$(BIN): $(BINDIR) $(OBJS)
	$(CC) $(OBJS) -o $@ $(LIBS)

$(BINDIR):
	mkdir $(BINDIR)

%.o: %.c
	$(CC) $(CFLAGS) $(INC) -c $< -o $@

clean:
	rm -rf $(BINDIR) $(OBJS)
